// author: Stephen J. Radachy
// see: https://docs.angularjs.org/api/ngResource/service/$resource
(function() {
    // create angular.services module and inject ngResource
    var services = angular.module('angular.services', ['ngResource']);

    // define BooksFactory service; be sure to inject the $resource dependency
    services.factory('BooksFactory',['$resource', function ($resource) {
        return $resource('./api/books',{},{
            // get all books
            query: { method: 'GET', isArray: true },
            // create new book
            save: { method: 'POST'}
        });
    }]);

    // define BookFactory service; be sure to inject the $resource dependency
    services.factory('BookFactory',['$resource', function ($resource) {
        return $resource('./api/books/:id',{},{
            // delete book
            delete: { method: 'DELETE', params: {id: '@id'}},
            // update book
            update: { method: 'PUT', params: {id: '@id', title: '@title'}}
        });
    }]);

})();