// author: Stephen J. Radachy
(function() {
    // create angular module; be sure to inject angular.services dependency,
    // otherwise we cannot use the resources in services.js
    var app = angular.module('angular',['angular.services'])
        .run(function($rootScope){
            // grabs online status from JS online variable.
            // defined in web-app/js/online.js and
            // web-app/js/offline.js
            // see /grails-app/views/manifest/index.gsp

            // $rootScope is how global variables are defined in Angular.JS
            // see: https://docs.angularjs.org/api/ng/type/$rootScope.Scope
            $rootScope.isOnline = online;
        });

    // define our controller; note the referenced dependencies
    // see: https://docs.angularjs.org/api/ng/service/$controller
    // $scope references this specific controller
    // BooksFactory and BookFactory reference the services defined
    // in web-app/js/services.js
    app.controller("mainCtrl",function($scope, $rootScope, BooksFactory, BookFactory){
        $scope.isOnline = $rootScope.isOnline;

        if($scope.isOnline){
            $scope.status = "Online";

            // get all books from the domain
            $scope.books = BooksFactory.query();

            // create a new book
            $scope.saveBook = function (bookTitle){
                BooksFactory.save({title: bookTitle}, function(){
                    $scope.books = BooksFactory.query();
                });
            };

            // update a book
            $scope.updateBook = function (bookID, bookTitle) {
              BookFactory.update({id: bookID},{title: bookTitle}, function(){
                 $scope.books = BooksFactory.query();
              });
            };

            // delete a book
            $scope.deleteBook = function(bookID) {
                BookFactory.delete({id: bookID}, function(){
                    $scope.books = BooksFactory.query();
                });

            };
        } else {
            $scope.status = "Offline";

            // code for offline services (indexedDB based) would go here
        }
    });

})();