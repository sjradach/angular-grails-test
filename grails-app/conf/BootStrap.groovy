// author: Stephen J. Radachy
import angular.Book
class BootStrap {

    def init = { servletContext ->
        // create sample books
        new Book(title: "test1").save()
        new Book(title: "test2").save()
        new Book(title: "test3").save()
        new Book(title: "test4").save()
        new Book(title: "test5").save()
        new Book(title: "test6").save()
    }
    def destroy = {
    }
}
