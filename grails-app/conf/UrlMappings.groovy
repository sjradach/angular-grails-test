// author: Stephen J. Radachy
class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        // implement REST with grails
        // see: http://grails.github.io/grails-doc/latest/guide/webServices.html#extendingRestfulController
        "/api/books"(resources:"book")
        "/"(view:"/index")
        "500"(view:'/error')
	}
}
