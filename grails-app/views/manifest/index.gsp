CACHE MANIFEST

# author: Stephen J. Radachy

CACHE:
# save these files locally for offline use
${request.contextPath}/js/vendor/angular.js
${request.contextPath}/js/vendor/angular-resource.js
${request.contextPath}/js/app.js
${request.contextPath}/js/services.js

FALLBACK:
# 'content-online' falls back to 'content-offline'
${request.contextPath} ${request.contextPath}/
${request.contextPath}/js/online.js ${request.contextPath}/js/offline.js

NETWORK:
*