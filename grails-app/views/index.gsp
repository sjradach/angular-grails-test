<!DOCTYPE html>
<!-- manifest for offline - see: grails-app/views/mainfest/index.gsp -->
<!-- also see: grails-app/controllers/angular/ManifestController -->
<html manifest="${request.contextPath}/manifest">
	<head>
		<meta name="author" content="Stephen J. Radachy">
		<title>Basic Angular + Grails Example</title>
		<!-- see web-app/js -->
		<!-- Angular.JS includes -->
		<g:external dir="js" file="vendor/angular.js" />
		<g:external dir="js" file="vendor/angular-resource.js" />
		<!-- Helps determine online status -->
		<g:external dir="js" file="online.js" />
		<!-- Angular app source files -->
		<g:external dir="js" file="app.js" />
		<g:external dir="js" file="services.js" />
	</head>
	<!-- Define Angular.JS module -->
	<body ng-app="angular">

	<h1 style="text-align:center;">Basic Angular + Grails Example</h1>

	<!-- Define mainCtrl Controller -->
	<div ng-controller="mainCtrl">
		<!-- Show online status -->
		<h3 style="text-align:center;" >
			Status: {{status}}
		</h3>

		<!-- Display books when online -->
		<form novalidate class="ng-show" ng-show="isOnline">
			<input type="text" ng-model="addbook"/>
			<input type="submit" ng-click="saveBook(addbook)" value="Add Book"/>
		</form><br><br>
		<!-- Gets all books and displays each ordered by ID -->
		<form novalidate class="ng-show" ng-show="isOnline" ng-repeat="book in books | orderBy : 'id'">
			<input type="text" ng-model="booktitle"/>

			<button ng-click="updateBook(book.id,booktitle)">Update</button>
			<button ng-click="deleteBook(book.id)">Delete</button>
			<strong>id:</strong> {{book.id}} <strong>title:</strong> {{book.title}}
		</form>
	</div>
	</body>
</html>
