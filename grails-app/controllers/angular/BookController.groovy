// author: Stephen J. Radachy

package angular
import grails.rest.RestfulController

// extended RestfulController
// see: http://grails.github.io/grails-doc/latest/guide/webServices.html#extendingRestfulController
class BookController extends RestfulController {
    // generate dynamic scaffolding
    static scaffold = true

    static responseFormats = ['json']

    BookController() {
        super(Book)
    }

    // respond with all books in domain
    def index(Integer max) {
        respond Book.list()
    }
}