package angular

class ManifestController {
    def index = {
        response.setContentType 'text/cache-manifest'
        response.setHeader('Cache-Control', 'max-age=0')
        response.setDateHeader('Expires', 0)
    }
}
