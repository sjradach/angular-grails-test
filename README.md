# Basic Angular.JS (1.3.15) + Grails (2.5.0) example

This is a proof-of-concept. It serves two purposes:

1. To show that it is possible to use Angular.JS (1.3.15) in concert with Grails (2.5.0)
2. To show that global Javascript variables can be leveraged within Angular.JS

# Overview

## How to integrate a Grails domain with Angular.JS

1. Create your Grails domain class: grails create-domain-class Book
2. Modify the domain class to your specification
3. Create the associated controller: grails generate-all 'package'.Book
4. Have your domain controller import and extend grails.rest.RestfulController
5. Set the response format to JSON
6. Add a REST urimapping for your domain - see grails-app/conf/UriMappings.groovy
7. Create your associated Angular.JS resource(s) - see web-app/js/services.js
8. Inject your resources module into your main module and controller(s) - see web-app/js/app.js
9. Add code to connect the resource(s) within your controller(s)
10. Reference your controller(s) and variables within a HTML file - see /grails-app/views/index.gsp

### Relevant Files

* grails-app/conf/UrlMappings.groovy
* grails-app/conf/BootStrap.groovy
* grails-app/controllers/angular/BookController.groovy
* grails-app/domain/angular/Book.groovy
* grails-app/views/index.gsp
* web-app/js/vendor/angular.js
* web-app/js/vendor/angular-resource.js
* web-app/js/app.js
* web-app/js/services.js

##  How to use global JS variables within Angular.JS (online vs offline)

1. Create a manifest and reference it within your HTML source
    see: /grails-app/views/index.gsp, /grails-app/views/manifest/index.gsp, /grails-app/controllers/ManifestController.groovy
2. Create two files (online.js, offline.js) and use offline.js as a fallback for online.js in the manifest
3. Include online.js in your HTML file before your Angular.JS includes
4. Set a $rootScope variable within Angular.JS - see /web-app/js/app.js
5. Reference $rootScope within your controller

### Relevant Files

* grails-app/controllers/ManifestController.groovy
* grails-app/views/manifest/index.gsp
* grails-app/views/index.gsp
* web-app/js/vendor/angular.js
* web-app/js/vendor/angular-resource.js
* web-app/js/app.js
* web-app/js/online.js
* web-app/js/offline.js